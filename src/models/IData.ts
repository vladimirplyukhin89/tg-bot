export interface IToken {
  brand: object[] // id_in_db  - number
  category?: string // chat_id - number
  rating?: number // number_ls - number
}

interface IServices {
  name: string
  consumption: string | null
  oid: number
}

interface IService {
  name: string
  oid: number
}

interface ICurrent {
  value: string
  date: string
  type: string
}

interface IPrevious {
  value: string
  date: string
  type: string
}

interface ICounters {
  oid: number
  service: IService | null
  unit: string
  current: ICurrent | null
  previous: IPrevious | null
  serial: string
  model?: string | null
  location?: string
  created_at: string
  updated_at: string
  next_verification?: string
  source: string
}

interface IData {
  account: number
  counters?: ICounters[]
  services?: IServices[]
}

const data: IData[] = [
  {
    account: 5555555,
    counters: [
      {
        oid: 687313859,
        service: {
          name: "Электричество",
          oid: 1,
        },
        unit: "кВт.ч",
        current: {
          value: "14500",
          date: "12.09.2022",
          type: "vostok",
        },
        previous: {
          value: "14500",
          date: "12.09.2022",
          type: "vostok",
        },
        serial: "15002763",
        created_at: "2022-01-19 11:05:57",
        updated_at: "2022-09-12 13:37:16",
        source: "vostok",
      },
      {
        oid: 348419,
        unit: "м³",
        serial: "25257989",
        model: "СГВ 15",
        location: "Ванная",
        current: null,
        previous: {
          value: "602",
          date: "25.07.2022",
          type: "abonent",
        },
        service: {
          name: "Холодное водоснабжение",
          oid: 2,
        },
        created_at: "2018-10-03 17:20:59",
        updated_at: "2018-10-03 17:20:59",
        next_verification: "30.07.2024",
        source: "itpc",
      },
      {
        oid: 348420,
        unit: "м³",
        serial: "25398903",
        model: "СГВ 15",
        location: "Кухня",
        current: null,
        previous: {
          value: "36",
          date: "25.07.2022",
          type: "abonent",
        },
        service: {
          name: "Холодное водоснабжение",
          oid: 2,
        },
        created_at: "2018-10-03 17:20:59",
        updated_at: "2022-03-12 08:52:36",
        next_verification: "30.07.2024",
        source: "itpc",
      },
      {
        oid: 1900981,
        unit: "м³",
        serial: "0001382400",
        model: null,
        location: "Ванная",
        current: null,
        previous: {
          value: "20",
          date: "25.07.2022",
          type: "abonent",
        },
        service: {
          name: "Горячее водоснабжение",
          oid: 3,
        },
        created_at: "2020-12-17 16:49:34",
        updated_at: "2020-12-17 16:49:34",
        next_verification: "02.10.2026",
        source: "itpc",
      },
      {
        oid: 1900982,
        unit: "м³",
        serial: "0001382340",
        model: null,
        location: "Кухня",
        current: null,
        previous: {
          value: "15",
          date: "25.07.2022",
          type: "abonent",
        },
        service: {
          name: "Горячее водоснабжение",
          oid: 3,
        },
        created_at: "2020-12-17 16:49:34",
        updated_at: "2020-12-17 16:49:34",
        next_verification: "02.10.2026",
        source: "itpc",
      },
    ],
    services: [
      {
        name: "Электричество",
        consumption: "40",
        oid: 1,
      },
      {
        name: "Холодное водоснабжение",
        consumption: null,
        oid: 2,
      },
      {
        name: "Горячее водоснабжение",
        consumption: null,
        oid: 3,
      },
    ],
  },
]
