// Для реального проекта
//export interface ICounters {
//  img: string
//  title: string
//  subtitle: string
//  indicators: string
//  id: number
//}

export interface ICounters {
  result: object
}

export interface IError {
  error: string
}
