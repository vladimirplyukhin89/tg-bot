import { sendHttpRequest } from "./../helpers"
import { URL } from "./../constants"
import { ICounters, IError } from "./../models"

export const putData = async (): Promise<object | string> => {
  try {
    const responseData: Response = await sendHttpRequest(
      "PUT",
      "https://reqres.in/api/register",
      {}
    )
    console.log(responseData)
    return responseData
  } catch (e: any) {
    const error: string = e
    console.error(error)
    return error
  }
}
