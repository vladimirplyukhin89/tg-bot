import { sendHttpRequest } from "../helpers/sendHttpRequest"
import { URL } from "./../constants"

export const getData = async (): Promise<object | string> => {
  try {
    const responseData: Response = await sendHttpRequest(
      "GET",
      "https://reqres.in/api/users",
      {},
      1,
      2,
      3
    )
    console.log(responseData)
    return responseData
  } catch (e: any) {
    const error: string = e
    console.error(error)
    return error
  }
}
