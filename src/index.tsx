import ReactDOM from "react-dom"
import "bootstrap/dist/css/bootstrap.css"

import "./styles/styles.scss"
import { App } from "./components/App"

if (document.getElementById("app")) {
  ReactDOM.render(
    <div>
      <App />
    </div>,
    document.getElementById("app")
  )
}
