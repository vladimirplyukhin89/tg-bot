import { ITitle } from "./../../../../models"

export const Title: React.FC<ITitle> = ({ title }) => {
  return (
    <div className="d-flex align-self-start title">
      <h2>{title}</h2>
    </div>
  )
}
