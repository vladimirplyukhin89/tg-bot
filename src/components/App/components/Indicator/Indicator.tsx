import DropRed from "media/DropRed.svg"

export const Indicator: React.FC = () => {
  return (
    <div className="container-fluid section">
      <div className="row px-3 py-3 align-items-center section__line">
        <div className="col-1 px-0">
          <img src={DropRed} alt="Lightning" width="20" height="20" />
        </div>

        <div className="col-7 px-0">
          <div className="section__block">
            <h3 className="mb-1 section__block-title">Горячая вода</h3>
            <p className="mb-0 section__block-subtitle">Поверка 25.03.2024</p>
            <p className="mb-0 section__block-subtitle">№ 06 7289850</p>
          </div>
        </div>

        <div className="col-4 px-0 d-flex flex-column section__counter">
          <div className="d-inline-flex flex-row section-wrapper">
            <input
              type="text"
              value={0}
              className="mb-0 section-wrapper__input"
            />
            <span className="section-wrapper__meter">
              м<sup>3</sup>
            </span>
          </div>
          <p className="mb-0 section__counter-date">за 25.03.2024</p>
        </div>
      </div>
    </div>
  )
}
