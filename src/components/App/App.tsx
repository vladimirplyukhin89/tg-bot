import React from "react"

import { CounterList } from "./components/CounterList/CounterList"
import { Indicator } from "./components/Indicator"
import { Title } from "./components/Title"
import { Button } from "./components/Button"
import { getData, putData } from "./../../api"

import { ICounters } from "models"

import Lightning from "media/Lightning.svg"
import Drop from "media/Drop.svg"
import Thermometer from "media/Thermometer_dark@2x.svg"

interface IJson {
  account: number
  counters?: Array<{}>
  services?: IServices[]
}

interface IServices {
  name: string
  consumption: string | null
  oid: number
}

//interface ICounter {
//  array:
//}

export const App: React.FC = (data) => {
  return (
    <div className="container-fluid px-0">
      <div className="d-flex flex-column justify-content-center align-items-center pt-4 wrapper">
        <h1 className="m-0 wrapper__title">Счетчики</h1>
        {/*{data.map((list, index) => {
          return <CounterList />
        })}*/}
        {/*<Title title={"Ванная"} />
        <Indicator />
        <Title title={"Кухня"} />
        <Indicator />
        <Indicator />
        <Title title={"Межэтажный щит"} />
        <Indicator />
        <Title title={"Туалет"} />
        <Indicator />*/}
        <Button />
      </div>
    </div>
  )
}
