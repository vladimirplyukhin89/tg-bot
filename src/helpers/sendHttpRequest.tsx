import { ICounters, IError } from "../models"

export const sendHttpRequest = async (
  method: string,
  url: string,
  data?: object,
  id_in_db?: number,
  chat_id?: number,
  number_ls?: number
): Promise<any> => {
  const promise = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open(method, url)

    xhr.responseType = "json"

    if (data) {
      xhr.setRequestHeader("Content-Type", "application/json")
    }
    // Создаю переменные из токена и отправляю их в headers запросе
    id_in_db && xhr.setRequestHeader("id_in_db", String(id_in_db))
    chat_id && xhr.setRequestHeader("chat_id", String(chat_id))
    number_ls && xhr.setRequestHeader("number_ls", String(number_ls))

    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(xhr.response)
      } else {
        resolve(xhr.response)
      }
    }

    xhr.onerror = () => {
      reject("Ошибка при отправки запроса")
    }

    xhr.send(JSON.stringify(data))
  })
  return promise
}
